// JavaScript Document
$(document).ready(function() 
{
var articles = document.getElementById('innerwrapper').getElementsByClassName('article');
var length = articles.length;

	for(var i = 0; i < length; i++)
	{
		var article = articles[i];
		
		article.classList.toggle('hide');
	
		var header = article.getElementsByTagName('h2')[0];
		var data = header.innerHTML;
		
	//	Creating elements		
		var div = document.createElement('div');
		var para = document.createElement('p');
		var content = document.createTextNode(data);
		var hr = document.createElement('hr');
		var lnBreak = document.createElement('br');

		var sidebar = document.getElementById('sidebar');		

	//	Appending the children nodes to parent nodes.
		div.appendChild(para);
		para.appendChild(content);
		
		div.addEventListener('click', toggleHide, false);
		div.style.backgroundColor = "white";
		hr.style.width = "inherit";
		sidebar.style.border = "thick solid #617B96";		
				
		sidebar.appendChild(div);
		sidebar.appendChild(hr);
	}
});

function fadeIn(div)
{
	for(var j = 0; j < 1000; j++)
	{
		div.style.opacity = parseFloat(div.style.opacity) + 0.001;
	}
}

function toggleHide(){
	var headerSide = this.getElementsByTagName('p')[0];
	
	var articles = document.getElementsByClassName('article');
	var length = articles.length;

	for(var i = 0; i < length; i++)
	{
		var article = articles[i];
		
		var header = article.getElementsByTagName('h2')[0];
	
		if(headerSide.innerHTML == header.innerHTML)
		{
			article.classList.toggle('hide');
			return;
		}
	}
}