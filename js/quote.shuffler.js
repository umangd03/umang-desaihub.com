// JavaScript Document

var quotes = [];
var i=0;

quotes.push('&ldquo; We are what we repeatedly do. Excellence, then, is not an act but a habit. &rdquo;<br>-Aristotle',
		 	'&ldquo; It has become appallingly obvious that our technology has exceeded our humanity. &rdquo;<br>- Albert Einstein' /*Quote1*/,
			'&ldquo; It is not necesssary to understand things in order to argue about them. &rdquo;<br>-Caron de Beaumarchais' /*Quote2*/,
			'&ldquo; Always go to other people\'s funerals, otherwise they won\'t come to yours. &rdquo;<br>-Yogi Berra' /*Quote3*/,
			'&ldquo; Without inspiration the best powers of the mind remain dormant. There is a fuel in us which needs to be ignited with sparks. &rdquo;<br>-Johann Gottfried Von \
					Herder' /*Quote4*/,
			'&ldquo; Try not to become a man of success but a man of value. &rdquo;<br>-Albert Einstein' /*Quote5*/,
			'&ldquo; The power of imagination makes us infinite. &rdquo;<br>-John Muir',/*Quote6*/
			'&ldquo; After the game, the king and the pawn go into the same box. &rdquo;<br>-Italian Proverb' /*Quote7*/,
			'&ldquo; We are what we repeatedly do. Excellence, then, is not an act but a habit. &rdquo;<br>-Aristotle');



function shuffle_quote(i)
{
	{
		$('.quote p').hide().fadeIn(3000).html("<p>" + quotes[i] 	
		+ "</p>");
	}
}

$(document).ready(function() 
{
	$('.quote p').fadeIn(3000).html("<p>" + quotes[0] + "</p>");
	
	setInterval(function() {i++; if (i >= 0) { shuffle_quote(i) } if(i==quotes.length-1){i=0;}}, 7000);

});

